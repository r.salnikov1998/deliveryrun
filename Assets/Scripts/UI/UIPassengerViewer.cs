﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIPassengerViewer : MonoBehaviour
{
    [SerializeField] private List<Image> _passengerIcons;
    [SerializeField] private Player _player;

    private Image _currentIcon;

    private void OnEnable()
    {
        _player.PassengerEntered += OnPassengerEntered;
        _player.PassengerOut += OnPassengerOut;
    }

    private void OnDisable()
    {
        _player.PassengerEntered -= OnPassengerEntered;
        _player.PassengerOut -= OnPassengerOut;
    }

    private void OnPassengerEntered()
    {
        _currentIcon = _passengerIcons.FirstOrDefault();
        _currentIcon.GetComponent<Image>().color = Color.yellow;
    }

    private void OnPassengerOut()
    {
        _currentIcon.GetComponent<Image>().color = Color.green;
        _passengerIcons.Remove(_currentIcon);
    }

}
