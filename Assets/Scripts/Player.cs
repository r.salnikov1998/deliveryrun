﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody), typeof(SplineFollower))]
public class Player : MonoBehaviour
{
    [SerializeField] private float _speedValue;

    [SerializeField] private float _lowReceivedMoney;
    [SerializeField] private float _maxReceivedMoney;

    [SerializeField] private float _waitingPickupTime;
    [SerializeField] private SplineFollower _splineFollower;
    [SerializeField] private List<Passenger> _availablePassengers;

    private Passenger _currentPassenger;
    private bool _canMove = true;
    private float _money;

    public event Action PassengerEntered;
    public event Action PassengerOut;
    public event Action<float> MoneyChanged;

    private void OnDisable()
    {
        if (_currentPassenger == null)
        {
            Debug.Log("Cant subscribe!! Current passenger is Null");
            return;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out PickupIcon pickupIcon))
        {
            _currentPassenger = _availablePassengers.FirstOrDefault();
            _currentPassenger.LookAtPlayer();
            _currentPassenger.MoveToPlayer();

            PassengerEntered?.Invoke();

            StartCoroutine(WaitingPassenger());
        }

        if (other.TryGetComponent(out DestinationIcon destinationIcon))
        {
            _currentPassenger.GetComponent<Collider>().enabled = false;
            _currentPassenger.Activate();
            _currentPassenger.MoveToDestination();

            _money += Random.Range(_lowReceivedMoney, _maxReceivedMoney);
            MoneyChanged?.Invoke(_money);
            PassengerOut?.Invoke();

            StartCoroutine(WaitingPassenger());
            _availablePassengers.Remove(_currentPassenger);
            _currentPassenger = null;
        }

        if (other.TryGetComponent(out TrafficCar trafficCar))
        {
            trafficCar.StopMovement();
            StopMovement();
        }
    }

    private void Update()
    {
        if (_canMove)
        {
            Move();
        }
    }

    private void Move()
    {
        const float speedRatio = 2;

        if (Input.GetMouseButton(0))
        {
            if (_splineFollower.followSpeed >= _speedValue)
            {
                return;
            }

            _splineFollower.followSpeed += _speedValue * speedRatio * Time.deltaTime;
        }
        else
        {
            _splineFollower.followSpeed -= _speedValue * speedRatio * Time.deltaTime;
        }
    }

    private IEnumerator WaitingPassenger()
    {
        StopMovement();
        yield return new WaitForSeconds(_waitingPickupTime);
        AllowMovement();
    }

    private void StopMovement()
    {
        _splineFollower.followSpeed = 0;
        _canMove = false;
    }

    private void AllowMovement()
    {
        _canMove = true;
    }
}