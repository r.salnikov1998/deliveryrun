﻿using Dreamteck.Splines;
using UnityEngine;

[RequireComponent(typeof(SplineFollower))]

public class TrafficCar : MonoBehaviour
{
    [SerializeField] private SplineFollower _splineFollower;

    public void StopMovement()
    {
        _splineFollower.followSpeed = 0;
    }
}
