﻿using UnityEngine;

[ExecuteAlways]
public class CameraTracker : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private Vector3 _offsetPosition;
    [SerializeField] private Vector3 _offsetRotation;

    private void LateUpdate()
    {
        transform.rotation = Quaternion.Euler(_offsetRotation);
        transform.position = _target.position + _offsetPosition;
    }

}
